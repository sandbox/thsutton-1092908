Copyright
=========

This module makes it a little simpler to implement the copyright notice in
your site footer or as a block.

In essence, it provides a few settings for copyright owner and start date and
uses a few theme functions to add this to your footer and/or display it in a
block.
